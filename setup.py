from setuptools import setup, find_packages

setup(
    name="dln-downloader",
    version="0.1",
    packages=find_packages(),
    # py_modules=["dln_downloader.episode", "dln_downloader.podcast", "dln_downloader.downloader"],
    # py_modules=["main"],
    install_requires=["docopt"],
    python_requires=">=3.7",
    entry_points="""
        [console_scripts]
        dln-downloader=main:main
    """,
)
