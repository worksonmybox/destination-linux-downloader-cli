"""DLN-Downloader

Usage:
  dln-downloader debug
  dln-downloader -h | --help
  dln-downloader --version
  dln-downloader --show-names
  dln-downloader --soft-link
  dln-downloader download-all [-l | --latest] [-a | --all] [--latest-range LATEST_RANGE] [--descending] [debug]
  dln-downloader download SHOW_NAME [-l | --latest] [-a | --all] [--latest-range LATEST_RANGE] [-e EPISODE_NUMBER | --episode EPISODE_NUMBER] [-r EPISODE_RANGE | --range EPISODE_RANGE] [--descending] [debug]

Options:
  -h, --help                                   Show this screen
  --version                                    Show version
  --show-names                                 Display show names
  --soft-link                                  A flag that allows the user to create a soft link to the Podcasts directory in their home folder
  -l, --latest                                 A flag for getting the latest episode of a given podcast or all podcasts e.g. dln-downloader download dlnx -l
  --latest-range LATEST_RANGE                  A flag for getting the latest episode(s) of a given podcast where LATEST_RANGE is a number e.g. dln-downloader download-all --latest-range 3
  -a, --all                                    A flag for getting all episodes of a given show, default False
  -e EPISODE_NUMBER, --episode EPISODE_NUMBER  A flag for setting a specific episode number to download, default None
  -r EPISODE_RANGE, --range EPISODE_RANGE      A flag for downloading a range of episodes where EPISODE_RANGE is a number X..Y e.g. dln-downloader download ans -r 200..205
  --descending                                 A flag for setting the order in which podcasts are downloaded, default False (episodes are downloaded in the order they were released)
  -v                                           A flag to set verbosity to gather more information/logging, default False
  --vv                                         A flag to set double verbosity to gather more information/logging, default False

"""
from docopt import docopt
from dln_downloader.downloader import DLNDownloader


def main():
    """
    Main function for running the Docopt CLI
    """
    arguments = docopt(__doc__, version="DLN-Downloader 0.1")
    d = DLNDownloader()
    show_banner()

    if arguments.get("debug"):
        print(arguments)
    if arguments.get("--show-names"):
        print("Shows: ")
        print("  Ask Noah Show (ans)")
        print("  Destination Linux (dl)")
        print("  DLN Xtend (dlnx)")
        print("  Game Sphere (gs)")
        print("  Hardware Addicts (ha)")
        print("  Sudo Show (ss)")
        print("  This Week in Linux (twil)")
    elif arguments.get("download"):
        show_name = d.validate_show_name(arguments.get("SHOW_NAME"))

        if arguments.get("--episode"):
            if arguments.get("--latest"):
                raise ValueError("Cannot use --episode and --latest flags together")
            if arguments.get("--all"):
                raise ValueError("Cannot use --episode and --all flags together")
            if arguments.get("--latest-range"):
                raise ValueError(
                    "Cannot use --episode and --latest-range flags together"
                )
            if arguments.get("--range"):
                raise ValueError("Cannot use --episode and --range flags together")
            for podcast in d.podcasts:
                if show_name == str(podcast):
                    d.download(podcast, episode=arguments.get("--episode"))
                    break
        elif arguments.get("--latest"):
            if arguments.get("--episode"):
                raise ValueError("Cannot use --latest and --episode flags together")
            if arguments.get("--all"):
                raise ValueError("Cannot use --latest and --all flags together")
            if arguments.get("--latest-range"):
                raise ValueError(
                    "Cannot use --latest and --latest-range flags together"
                )
            if arguments.get("--range"):
                raise ValueError("Cannot use --latest and --range flags together")
            for podcast in d.podcasts:
                if show_name == str(podcast):
                    d.download(podcast, latest=True)
                    break
        elif arguments.get("--latest-range"):
            if arguments.get("--episode"):
                raise ValueError(
                    "Cannot use --latest-range and --episode flags together"
                )
            if arguments.get("--all"):
                raise ValueError("Cannot use --latest-range and --all flags together")
            if arguments.get("--latest"):
                raise ValueError(
                    "Cannot use --latest-range and --latest flags together"
                )
            if arguments.get("--range"):
                raise ValueError("Cannot use --latest-range and --range flags together")
            for podcast in d.podcasts:
                if show_name == str(podcast):
                    print(arguments.get("--latest-range"))
                    d.download(
                        podcast,
                        latest_range=arguments.get("--latest-range"),
                        descending=arguments.get("--descending"),
                    )
                    break
        elif arguments.get("--range"):
            if arguments.get("--episode"):
                raise ValueError("Cannot use --range and --episode flags together")
            if arguments.get("--all"):
                raise ValueError("Cannot use --range and --all flags together")
            if arguments.get("--latest"):
                raise ValueError("Cannot use --range and --latest flags together")
            if arguments.get("--latest-range"):
                raise ValueError("Cannot use --range and --latest-range flags together")
            for podcast in d.podcasts:
                if show_name == str(podcast):
                    d.download(
                        podcast,
                        episode_range=arguments.get("--range"),
                        descending=arguments.get("--descending"),
                    )
                    break
        elif arguments.get("--all"):
            if arguments.get("--episode"):
                raise ValueError("Cannot use --all and --episode flags together")
            if arguments.get("--latest"):
                raise ValueError("Cannot use --all and --latest flags together")
            if arguments.get("--latest-range"):
                raise ValueError("Cannot use --all and --latest-range flags together")
            if arguments.get("--range"):
                raise ValueError("Cannot use --all and --range flags together")
            for podcast in d.podcasts:
                if show_name == str(podcast):
                    d.download(
                        podcast, all_=True, descending=arguments.get("--descending")
                    )
                    break
    elif arguments.get("download-all"):
        if arguments.get("--latest"):
            if arguments.get("--all"):
                raise ValueError("Cannot use --latest and --all flags together")
            if arguments.get("--latest-range"):
                raise ValueError(
                    "Cannot use --latest and --latest-range flags together"
                )
            d.download_latest_episodes()
        if arguments.get("--all"):
            if arguments.get("--latest"):
                raise ValueError("Cannot use --all and --latest flags together")
            if arguments.get("--latest-range"):
                raise ValueError("Cannot use --all and --latest-range flags together")
            d.download_all_episodes(descending=arguments.get("--descending"))
        if arguments.get("--latest-range"):
            if arguments.get("--latest"):
                raise ValueError(
                    "Cannot use --latest-range and --latest flags together"
                )
            if arguments.get("--all"):
                raise ValueError("Cannot use --latest-range and --all flags together")
            for podcast in d.podcasts:
                d.download(
                    podcast,
                    latest_range=arguments.get("--latest-range"),
                    descending=arguments.get("--descending"),
                )
    elif arguments.get("--soft-link"):
        d.create_soft_link()

def get_colors():
    return {
        "black": "\u001b[30m",
        "red": "\u001b[31m",
        "green": "\u001b[32m",
        "yellow": "\u001b[33m",
        "blue": "\u001b[34m",
        "magenta": "\u001b[35m",
        "cyan": "\u001b[36m",
        "white": "\u001b[37m",
        "reset": "\u001b[0m",
    }

def show_banner():
    colors = get_colors()

    print(" ____  _     _   _ ")
    print("|  _ \| |   | \ | |")
    print("| | | | |   |  \| |")
    print("| |_| | |___| |\  |")
    print("|____/|_____|_| \_|\n")




if __name__ == "__main__":
    main()
