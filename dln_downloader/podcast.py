from dln_downloader.episode import Episode


class Podcast:
    """Represents a podcast."""

    name = ""
    description = ""
    website = ""
    rss_link = ""
    rss = ""
    episodes = []

    def __init__(self, name: str, rss_link: str = "", rss: str = "", website: str = ""):
        """Initializes the data."""
        self.name = name
        self.rss_link = rss_link
        self.rss = rss
        self.website = website

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.name!r})"

    def __str__(self) -> str:
        return f"{self.name}"
