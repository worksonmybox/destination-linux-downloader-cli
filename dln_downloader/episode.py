class Episode:
    """Represents a single podcast episode."""

    name = ""
    number = 0
    description = ""
    summary = ""
    link = ""
    links = []

    def __init__(
        self,
        name: str,
        number: int = 0,
        description: str = "",
        summary: str = "",
        link: str = "",
        links: list = [],
    ):
        """Initializes the data."""
        self.name = name

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.name!r})"

    def __str__(self) -> str:
        return f"{self.name}"
