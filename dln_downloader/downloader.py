from dln_downloader.podcast import Podcast
from dln_downloader.episode import Episode
import feedparser
from pathlib import Path
import subprocess
import os
import re

# for use later when I start caching and doing async
# import json
# import aiohttp
# import asyncio


class DLNDownloader:
    """Represents a podcast downloading tool for the DLN network."""

    podcasts = []
    parser = feedparser
    pwd = Path(__file__).resolve().parent.parent
    podcasts_dir = Path(pwd / "Podcasts").resolve()
    podcasts_dir.mkdir(exist_ok=True)

    def __init__(self):
        podcasts = []
        podcasts.append(
            Podcast("Destination Linux", "https://destinationlinux.org/feed/mp3")
        )
        podcasts.append(Podcast("Ask Noah Show", "https://podcast.asknoahshow.com/rss"))
        podcasts.append(
            Podcast(
                "This Week in Linux", "https://tuxdigital.com/feed/thisweekinlinux-mp3"
            )
        )
        podcasts.append(Podcast("Hardware Addicts", "https://hardwareaddicts.org/rss"))
        podcasts.append(Podcast("Sudo Show", "https://sudo.show/rss"))
        podcasts.append(Podcast("DLN Xtend", "https://dlnxtend.com/rss"))
        podcasts.append(Podcast("Game Sphere", "https://gamesphere.show/rss"))

        self.podcasts = podcasts

        for podcast in podcasts:
            podcast_dir = self.podcasts_dir / str(podcast)
            podcast_dir.mkdir(exist_ok=True)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}()"

    def __str__(self) -> str:
        return f"DLN podcast downloader with {self.podcasts}."

    def validate_show_name(self, showname: str) -> str:
        """Validates a given showname and converts from shorthand to show name as necessary"""

        if showname == "ans" or showname == "Ask Noah Show":
            showname = "Ask Noah Show"
        elif showname == "dl" or showname == "Destination Linux":
            showname = "Destination Linux"
        elif showname == "dlnx" or showname == "DLN Xtend":
            showname = "DLN Xtend"
        elif showname == "gs" or showname == "Game Sphere":
            showname = "Game Sphere"
        elif showname == "ha" or showname == "Hardware Addicts":
            showname = "Hardware Addicts"
        elif showname == "twil" or showname == "This Week in Linux":
            showname = "This Week in Linux"
        elif showname == "ss" or showname == "Sudo Show":
            showname = "Sudo Show"
        else:
            raise ValueError(f"No show by the name {showname} exists.")
        return showname

    def update_rss_cache(self, podcast: Podcast) -> None:
        """Updates the RSS cache for a given show"""

        rss_feed = self.parser.parse(podcast.rss_link)
        print(f"\n[i] Updating cache for {podcast.rss_link}")
        os.chdir(Path(self.podcasts_dir / str(podcast)))
        subprocess.run(
            ["curl", "-L", f"{podcast.rss_link}", "-o", f"{str(podcast)} RSS.txt"]
        )
        os.chdir(self.pwd)

    def create_soft_link(self) -> None:
        """Creates a soft link from the Podcasts directory created by this program to the users home directory called Podcasts"""

        podcast_link = Path().home() / "Podcasts"
        podcast_link.resolve()
        if (
            not podcast_link.is_symlink()
            and not podcast_link.is_file()
            and not podcast_link.is_dir()
        ):
            subprocess.run(["ln", "-s", self.podcasts_dir, Path().home()])
        else:
            print(f"[!] {podcast_link} already exists, cannot create.")

    def get_episode_title(self, podcast: Podcast, entries: list, episode: str) -> str:
        """Gets the episode title by utilizing python regex functionality and returns the title if matched"""

        pattern = re.compile(r"\d?\d?\d?:+")
        match = ""
        try:
            episode = int(episode)
        except ValueError as e:
            print(e)
            print(
                f"Cannot cast episode {episode} to int, please input a valid episode number"
            )
        for entry in entries:
            match = pattern.findall(entry["title"])
            if match:
                new_pattern = re.compile(r"\d+\d?\d?")
                new_match = new_pattern.findall(match[0])
                new_match = int(new_pattern.findall(match[0])[0])
                if new_match == episode:
                    return entry["title"]
        raise ValueError(f"{str(podcast)} - {episode} does not exist")

    def validate_range(
        self, podcast: Podcast, episode_range: str, entries: list
    ) -> list:
        """Validates the episode range for a given podcast to ensure the range of episodes exist to download"""

        pattern = re.compile(r"[-]?\d+\d?\d?[.]{2}\d+\d?\d?")
        match = pattern.findall(episode_range)
        titles = []
        if match:
            split_matches = match[0].split("..")
            low = int(split_matches[0])
            high = int(split_matches[1])
            if low < 0:
                low = 0
            if high > len(entries):
                high = len(entries)
            for i in range(low, high + 1):
                titles.append(self.get_episode_title(podcast, entries, str(i)))
        return titles

    def download(
        self,
        podcast: Podcast,
        episode: str = "",
        latest: bool = False,
        latest_range: str = "",
        episode_range: str = "",
        all_: bool = False,
        descending: bool = False,
    ) -> None:
        """Downloads a podcast"""

        print(f"[i] Fetching RSS feed for {str(podcast)}...")
        rss_feed = self.parser.parse(podcast.rss_link)
        entries = [entry for entry in rss_feed["entries"]]
        feed_links = [entry["links"][1]["href"] for entry in entries]
        os.chdir(Path(self.podcasts_dir / str(podcast)))

        if episode:
            entries.reverse()
            feed_links.reverse()
            title = self.get_episode_title(podcast, entries, episode)
            for entry in entries:
                if entry["title"] == title:
                    if Path(
                        self.podcasts_dir / str(podcast) / entry["title"]
                    ).is_file():
                        self.print_exists(str(podcast), entry["title"])
                    else:
                        self.print_downloading(str(podcast), entry["title"])
                        subprocess.run(
                            [
                                "curl",
                                "-L",
                                f"{entry['links'][1]['href']}",
                                "-o",
                                f"{entry['title']}",
                            ]
                        )
                        break
        if latest:
            entries.reverse()
            feed_links.reverse()
            if Path(self.podcasts_dir / str(podcast) / entries[-1]["title"]).is_file():
                self.print_exists(str(podcast), entries[-1]["title"])
            else:
                self.print_downloading(str(podcast), entries[-1]["title"])
                subprocess.run(
                    ["curl", "-L", f"{feed_links[-1]}", "-o", f"{entries[-1]['title']}"]
                )
        if latest_range:
            if not descending:
                for i in range(int(latest_range) - 1, -1, -1):  # 205, 206, 207
                    if i >= len(entries):
                        continue
                    if Path(
                        self.podcasts_dir / str(podcast) / entries[i]["title"]
                    ).is_file():
                        self.print_exists(str(podcast), entries[i]["title"])
                    else:
                        self.print_downloading(str(podcast), entries[i]["title"])
                        subprocess.run(
                            [
                                "curl",
                                "-L",
                                f"{feed_links[i]}",
                                "-o",
                                f"{entries[i]['title']}",
                            ]
                        )
            else:
                for i in range(0, int(latest_range)):  # 207, 206, 205
                    if i >= len(entries):
                        continue
                    if Path(
                        self.podcasts_dir / str(podcast) / entries[i]["title"]
                    ).is_file():
                        self.print_exists(str(podcast), entries[i]["title"])
                    else:
                        self.print_downloading(str(podcast), entries[i]["title"])
                        subprocess.run(
                            [
                                "curl",
                                "-L",
                                f"{feed_links[i]}",
                                "-o",
                                f"{entries[i]['title']}",
                            ]
                        )
        if episode_range:
            entries.reverse()
            titles = self.validate_range(podcast, episode_range, entries)
            if descending:
                titles.reverse()
            for title in titles:
                if Path(self.podcasts_dir / str(podcast) / title).is_file():
                    self.print_exists(str(podcast), title)
                else:
                    self.print_downloading(str(podcast), title)
                    for i, e in enumerate(entries):
                        if title == e["title"]:
                            subprocess.run(
                                [
                                    "curl",
                                    "-L",
                                    f"{feed_links[i]}",
                                    "-o",
                                    f"{entries[i]['title']}",
                                ]
                            )
                            break
        if all_:
            if not descending:
                entries.reverse()
            for i, link in enumerate(feed_links):
                if Path(
                    self.podcasts_dir / str(podcast) / entries[i]["title"]
                ).is_file():
                    self.print_exists(str(podcast), entries[i]["title"])
                else:
                    self.print_downloading(str(podcast), entries[i]["title"])
                    subprocess.run(
                        ["curl", "-L", f"{link}", "-o", f"{entries[i]['title']}"]
                    )
        os.chdir(self.pwd)

    def print_exists(self, podcast_name: str, title: str) -> None:
        """Prints whether a podcast exists in the Podcasts directory"""

        print(f"[i] {podcast_name} - {title} already exists...skipping")

    def print_downloading(self, podcast_name: str, title: str) -> None:
        """Prints whether a podcast is currently being downloaded"""

        print(f"[i] Downloading {podcast_name} - {title}")

    def download_latest_cached(self, podcast: Podcast) -> None:
        """Downloads the latest podcasts using the cache on disk"""
        pass

    def download_latest(self, podcast_name: str) -> None:
        """Downloads the latest podcast given a podcast name"""

        for podcast in self.podcasts:
            if str(podcast) == podcast_name:
                self.download(podcast, latest=True)
                break

    def download_latest_episodes(self) -> None:
        """Downloads the latest episodes of every podcast"""

        for podcast in self.podcasts:
            self.download_latest(str(podcast))

    def download_all_episodes(self, descending: bool = False) -> None:
        """Downloads all episodes of all podcasts"""

        for podcast in self.podcasts:
            self.download(podcast, all_=True, descending=descending)

    def update_podcast_cache(self, podcast: Podcast) -> None:
        """Updates the podcast cache on disk for future use"""
        pass

    def update_all_podcast_caches(self) -> None:
        """Updates all podcast caches on disk for future use"""
        pass
